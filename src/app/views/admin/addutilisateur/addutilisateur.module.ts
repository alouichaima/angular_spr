import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddutilisateurRoutingModule } from './addutilisateur-routing.module';
import { AddutilisateurComponent } from './addutilisateur.component';


@NgModule({
  declarations: [
    AddutilisateurComponent
  ],
  imports: [
    CommonModule,
    AddutilisateurRoutingModule
  ]
})
export class AddutilisateurModule { }
