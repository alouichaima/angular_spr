import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventsService } from 'src/app/service/events.service';

@Component({
  selector: 'app-addevents',
  templateUrl: './addevents.component.html',
  styleUrls: ['./addevents.component.css']
})
export class AddeventsComponent implements OnInit {

date: Date=new Date();
url:any ="";


  eve: any={'nomE':'','description':'', 'dateE':this.date ,'image':""};
  constructor( private service:EventsService , private router:Router
    ) { }




  ngOnInit(): void {
  //   this.eventService.getEvents().subscribe((response) => {
  //     this.listEvents = response

  //   }, (error) => {
  //     console.log(error);

  //   })

  }
  selectFile(event:any){
    if(!event.target.files[0] || event.target.files[0].length==0){
      return ;
    }
    let mimeType = event.target.files[0].type;
    if(mimeType.match(/image\/*/)==null){
      return ;
    }
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event)=>{
      this.url =reader.result;
      console.log(this.url)
 }

}



  add(){
    console.log(this.eve);
    this.service.addevent(this.eve).subscribe({

       next: (data:any)=>{
         this.router.navigate (['admin/list'])

      },

      error: (e:any)=> console.error(e),

      complete:()=>{}

      })


}}
