import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditeventsRoutingModule } from './editevents-routing.module';
import { EditeventsComponent } from './editevents.component';


@NgModule({
  declarations: [
    EditeventsComponent
  ],
  imports: [
    CommonModule,
    EditeventsRoutingModule,
    FormsModule
  ]
})
export class EditeventsModule { }
