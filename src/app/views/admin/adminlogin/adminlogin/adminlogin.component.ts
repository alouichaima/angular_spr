import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/service/login.service';
import { TokenStorageService } from 'src/app/service/token-storage.service';

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent implements OnInit {

  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errr = '';
  roles: string[] = [];
 
  constructor(private LoginService: LoginService, private tokenSto: TokenStorageService,private route:Router) { }
  
  ngOnInit(): void {
    if (this.tokenSto.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenSto.getUser().roles;
    }
  }
  onSubmit(): void {
    this.LoginService.login(this.form).subscribe(
      data => {
        this.tokenSto.saveToken(data.accessToken);
        this.tokenSto.saveUser(data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenSto.getUser().roles;
        this.route.navigate(['/admin/dashboard'])
      },
      err => {
        this.errr = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }
  reloadPage(): void {
    window.location.reload();
  }

}
