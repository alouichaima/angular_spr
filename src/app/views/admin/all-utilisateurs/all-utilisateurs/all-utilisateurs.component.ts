import { Component, OnInit } from '@angular/core';
import { utilisateur } from 'src/app/models/utilisateur.model';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { UserCRUDService } from 'src/app/service/user-crud.service';

@Component({
  selector: 'app-all-utilisateurs',
  templateUrl: './all-utilisateurs.component.html',
  styleUrls: ['./all-utilisateurs.component.css']
})
export class AllUtilisateursComponent implements OnInit {

  roles: string[] = [];
  utilisateur?: utilisateur[];
  curuser: utilisateur = {};
  curIndex = -1;
  

  constructor(private UserCRUDService: UserCRUDService, private tokenStorage: TokenStorageService ) { }

  ngOnInit(): void {
    this.retrieveUS();
    this.roles = this.tokenStorage.getUser().roles;

  }

  retrieveUS(): void {
    this.UserCRUDService.getAll()
      .subscribe(
        data => {
          this.utilisateur = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveUS();
    this.curuser = {};
    this.curIndex = -1;
  }

  setActiveUS(utilisateur: utilisateur, index: number): void {
    this.curuser = utilisateur;
    this.curIndex = index;
  }
  deleteUser(): void {
    this.UserCRUDService.delete(this.curuser.id)
      .subscribe(
        response => {
          console.log(response);
         
        },
        error => {
          console.log(error);
        });
  }
  
  
}
