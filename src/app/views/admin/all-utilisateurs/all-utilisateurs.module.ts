import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllUtilisateursRoutingModule } from './all-utilisateurs-routing.module';
import { AllUtilisateursComponent } from './all-utilisateurs/all-utilisateurs.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AllUtilisateursComponent
  ],
  imports: [
    CommonModule,
    AllUtilisateursRoutingModule,
    FormsModule
  ]
})
export class AllUtilisateursModule { }
