import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  form: any = {};
  isSuccess = false;
  isFailed = false;
  errM = '';


  constructor(private registerService: LoginService , private route:Router ) { }

  ngOnInit(): void {

  }



  onSubmit(): void {
    this.registerService.register(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccess = true;
        this.isFailed = false;
        this.route.navigate(['/login']);
      },
      err => {
        this.errM = err.error.message;
        this.isFailed = true;
      }
    );
  }

}
