import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BodycombatComponent } from './bodycombat.component';

describe('BodycombatComponent', () => {
  let component: BodycombatComponent;
  let fixture: ComponentFixture<BodycombatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BodycombatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BodycombatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
