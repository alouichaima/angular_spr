import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyattackComponent } from './bodyattack.component';

describe('BodyattackComponent', () => {
  let component: BodyattackComponent;
  let fixture: ComponentFixture<BodyattackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BodyattackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyattackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
