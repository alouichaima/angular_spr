import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BodyattackRoutingModule } from './bodyattack-routing.module';
import { BodyattackComponent } from './bodyattack.component';


@NgModule({
  declarations: [
    BodyattackComponent
  ],
  imports: [
    CommonModule,
    BodyattackRoutingModule
  ]
})
export class BodyattackModule { }
