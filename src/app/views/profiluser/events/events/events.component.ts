import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventsService } from 'src/app/service/events.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  list:any;
  fileToUpload: File | null = null;
    // router: any;
    constructor(private service:EventsService  ,private router:Router) {

     }

    ngOnInit(): void {
      this.getall();
    


    }

    getall():void{

  this.service.getAll().subscribe({next: (data) => {

  this.list= data;

  console.log(data);

  },

  error: (e) => console.error(e)

  });

    }
}
