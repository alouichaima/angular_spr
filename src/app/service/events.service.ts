import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Events } from '../models/events';

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  private baseUrl = 'http://localhost:8087/events';




  constructor( private http:HttpClient) { }


   // tslint:disable-next-line:typedef
   getAll()
   {
     return this.http.get(this.baseUrl + '/all');
   }

   addevent(e:Events):Observable<object>{
     return this.http.post("http://localhost:8087/events" ,e ).pipe()

   }

  getEventsById(id: number): Observable<any>
   {
     return this.http.get(this.baseUrl  + id);
   }

   supprimer(id: number): Observable<any>
   {
     return this.http.delete(this.baseUrl +  "/" +id, { responseType: 'text' });
   }
   update(eve:Events): any
   {
     return this.http.put(this.baseUrl ,eve).pipe();
   }


   // tslint:disable-next-line:ban-types
  //  addevents(e: Events): Observable<Object>
  //  {
  //    return this.http.post(this.baseUrl + 'eventss', e);
  //  }


  //  updateEtudiant(e: Events): any
  //  {
  //    return this.http.put(this.baseUrl+ 'update', e);
  //  }





  uploadFile(file: File): Observable<HttpEvent<{}>> {
		const formdata: FormData = new FormData();
		formdata.append('file', file);
		const req = new HttpRequest('POST', '<Server URL of the file upload>', formdata, {
			  reportProgress: true,
			  responseType: 'text'
		});

		return this.http.request(req);
   }
}
