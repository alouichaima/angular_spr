import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { utilisateur } from '../models/utilisateur.model';

const baseUrl = 'http://localhost:8087/utilisateur';
@Injectable({
  providedIn: 'root'
})
export class UserCRUDService {


  constructor(private http: HttpClient) { }

  getAll(): Observable<utilisateur[]> {
    return this.http.get<utilisateur[]>(baseUrl+"/all");
  }

  get(id: any): Observable<utilisateur> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }


}
