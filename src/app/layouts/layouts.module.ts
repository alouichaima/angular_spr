import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminlayoutComponent } from './adminlayout/adminlayout.component';
import { UserlayoutComponent } from './userlayout/userlayout.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ProfillayoutComponent } from './profillayout/profillayout/profillayout.component';


@NgModule({
  declarations: [
    AdminlayoutComponent,
    UserlayoutComponent,
    ProfillayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ]
})
export class LayoutsModule { }
